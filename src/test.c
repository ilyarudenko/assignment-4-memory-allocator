//
// Created by Ilya Rudenko on 11.12.2022.
//

#include "test.h"
#include "mem.h"
#include "mem_internals.h"

struct block_header* block_get_header(void* contents) {
    return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}
static void* block_after( struct block_header const* block )              {
    return  (void*) (block->contents + block->capacity.bytes);
}

void test_result_print(struct test_result status) {
    printf("%s", status.name);
    if (status.stat == TEST_PASSED) {
        printf(" PASSED");
    } else {
        fprintf(stderr, "%s", " FAILED: ");
        fprintf(stderr, "%s", status.msg);
    }
    printf("\n");
}

struct test_result normal_allocation_test() {
    heap_init(TEST_BUFFER_SIZE*4);
    struct block_header* block1 = block_get_header(_malloc(TEST_BUFFER_SIZE));
    struct block_header* block2 = block_get_header(_malloc(TEST_BUFFER_SIZE));

    if (!block1 || !block2) {
        _free(block2->contents);
        _free(block1->contents);

        return (struct test_result) {
                .name = "Normal Allocation",
                .stat = TEST_FAILED,
                .msg = "blocks weren't allocated"
        };
    }

    if (block1->capacity.bytes != TEST_BUFFER_SIZE || block2->capacity.bytes != TEST_BUFFER_SIZE){

        _free(block2->contents);
        _free(block1->contents);
        return (struct test_result) {
                .name = "Normal Allocation",
                .stat = TEST_FAILED,
                .msg =  "allocated incorrect block size"
        };

    }

    if (block1->is_free || block2->is_free){

        _free(block2->contents);
        _free(block1->contents);

        return (struct test_result) {
                .name = "Normal Allocation",
                .stat = TEST_FAILED,
                .msg = "blocks aren't marked as in use"
        };
    }

    _free(block2->contents);
    _free(block1->contents);

    return (struct test_result) {
            .name = "Normal Allocation",
            .stat = TEST_PASSED,
    };
}

struct test_result one_block_free_test() {

    struct block_header* block1 = block_get_header(_malloc(TEST_BUFFER_SIZE));
    struct block_header* block2 = block_get_header(_malloc(TEST_BUFFER_SIZE));
    struct block_header* block3 = block_get_header(_malloc(TEST_BUFFER_SIZE));
    struct block_header* block4 = block_get_header(_malloc(TEST_BUFFER_SIZE));

    _free(block3->contents);

    if (!block3->is_free) {
        _free(block4->contents);
        _free(block3->contents);
        _free(block2->contents);
        _free(block1->contents);

        return (struct test_result) {
                .name = "Single block free",
                .stat = TEST_FAILED,
                .msg = "block is still in use"
        };
    }


    if (block1->is_free || block2->is_free || block4->is_free) {
        _free(block4->contents);
        _free(block2->contents);
        _free(block1->contents);

        return (struct test_result) {
                .name = "Single block free",
                .stat = TEST_FAILED,
                .msg = "wrong block free"
        };
    }

    _free(block4->contents);
    _free(block2->contents);
    _free(block1->contents);

    return (struct test_result) {
            .name = "Single block free",
            .stat = TEST_PASSED,
    };
}

struct test_result two_blocks_free_test() {

    struct block_header* block1 = block_get_header(_malloc(TEST_BUFFER_SIZE));
    struct block_header* block2 = block_get_header(_malloc(TEST_BUFFER_SIZE));
    struct block_header* block3 = block_get_header(_malloc(TEST_BUFFER_SIZE));
    struct block_header* block4 = block_get_header(_malloc(TEST_BUFFER_SIZE));

    _free(block3->contents);
    _free(block2->contents);

    if (!block2->is_free) {
        _free(block4->contents);
        _free(block2->contents);
        _free(block1->contents);

        return (struct test_result) {
                .name = "Double block free",
                .stat = TEST_FAILED,
                .msg = "block is still in use"
        };
    }

    if (block1->is_free || block4->is_free) {
        _free(block4->contents);
        _free(block1->contents);

        return (struct test_result) {
                .name = "Double block free",
                .stat = TEST_FAILED,
                .msg = "wrong block free"
        };
    }

    if (block2->next != block4) {

        _free(block4->contents);
        _free(block1->contents);
        return (struct test_result) {
                .name = "Double block free",
                .stat = TEST_FAILED,
                .msg = "blocks are not merged"
        };
    }

    block_size whole_buffer = size_from_capacity((block_capacity) {TEST_BUFFER_SIZE});

    if (block2->capacity.bytes != TEST_BUFFER_SIZE + whole_buffer.bytes) {
        _free(block4->contents);
        _free(block1->contents);

        return (struct test_result) {
                .name = "Double block free",
                .stat = TEST_FAILED,
                .msg = "blocks are not merged"
        };
    }

    _free(block4->contents);
    _free(block1->contents);

    return (struct test_result) {
            .name = "Double block free",
            .stat = TEST_PASSED,
    };
}


struct test_result new_region_extends_test() {
    struct block_header* big_block = block_get_header(_malloc(TEST_EXTRA_BIG_BUFFER_SIZE));

    if (!big_block) {
        return (struct test_result) {
                .name = "New region extends",
                .stat = TEST_FAILED,
                .msg = "buffer not allocated"
        };
    }

    if (big_block->capacity.bytes != TEST_EXTRA_BIG_BUFFER_SIZE) {
        _free(big_block->contents);
        return (struct test_result) {
                .name = "New region extends",
                .stat = TEST_FAILED,
                .msg = "buffer with wrong capacity allocated"
        };
    }

    _free(big_block->contents);
    debug_heap(stdout, HEAP_START);

    return (struct test_result) {
            .name = "New region extends",
            .stat = TEST_PASSED,
    };
}

struct test_result new_region_not_extends_test() {
    struct region r = alloc_region(block_after(HEAP_START), REGION_MIN_SIZE);
    if (region_is_invalid(&r)) {
        printf("Not allocated");
    }

    struct block_header* big_block1 = block_get_header(_malloc(TEST_EXTRA_BIG_BUFFER_SIZE));
    struct block_header* big_block2 = block_get_header(_malloc(TEST_EXTRA_BIG_BUFFER_SIZE));

    struct block_header* block3 = block_get_header(_malloc(TEST_BUFFER_SIZE));

    if (!block3) {
        _free(big_block2->contents);
        _free(big_block1->contents);

        return (struct test_result) {
                .name = "New region NOT extends",
                .stat = TEST_FAILED,
                .msg = "block is not allocated"
        };
    }

    _free(block3->contents);
    _free(big_block2->contents);
    _free(big_block1->contents);

    debug_heap(stdout, HEAP_START);

    return (struct test_result) {
            .name = "New region NOT extends",
            .stat = TEST_PASSED,
    };
}


void start_tests(){
    struct test_result res = normal_allocation_test();
    test_result_print(res);

    res = one_block_free_test();
    test_result_print(res);

    res = two_blocks_free_test();
    test_result_print(res);

    res = new_region_extends_test();
    test_result_print(res);

    res = new_region_not_extends_test();
    test_result_print(res);
}